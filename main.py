#First file using numpy
import numpy as np
print("Hello, you are using NumPy " + np.__version__)

a = np.array([1,2,3])
print("This is a NumPy array: " + str(a))
print("The sape of the array is: " + str(a.shape))
print("It has this data type: " + str(a.dtype))
print("It has this many dimentions: " + str(a.ndim))
print("This is its size: " + str(a.size))
print("This is the byte size of each item: " + str(a.itemsize))
print("You can modify NumPy arrays like Python lists for the most part")
print("Unlike a list, you can not append to a NumPy array, but you can change the existing values.")
a = a + np.array([4])
print("Though if you try to add to it like a list it will instead add that vaule to all items in the list: " + str(a))
print("Most math to NumPy arrays works like this, applying the math to the value in the list, never growing the list.")
