import numpy as np

a = np.array([[1,2,3,4], [5,6,7,8]])
print(a)

#You can index arrays like lists in python, but you have to define all dimentions for the array
b = a[0,1]
print(b)

b = a[0,:]
print(b)

b = a[:,1]
print(b)

# You can also do bool indexing
bool_inx = a > 2
print(bool_inx)
b = np.where(a>2, a, -1)
print(b)

#fancy indexing
c = np.array([23,54,12,34,15,16])
print(c)
d = [1,3,5]
print(c[d])

even = np.argwhere(a%2==0).flatten()
print(c[even])
