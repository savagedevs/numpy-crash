import numpy as np

a = np.arange(1,11)
print(a)
print(a.shape)
b = a.reshape((2,5))
print(b)
print(b.shape)
c = a[np.newaxis, :]
print(c)
print(c.shape)
d = a[:, np.newaxis]
print(d)
print(d.shape)
